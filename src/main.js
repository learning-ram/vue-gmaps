import Vue from 'vue'
import App from './App.vue'
import * as VueGoogleMaps from "vue2-google-maps";
import axios from './backend/vue-axios'

Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDy68pIgOYmyj20gBPD_35_kTwlRycNw78",
    libraries: "places,visualization" // necessary for places input
  }
});

new Vue({
  axios,
  render: h => h(App),
}).$mount('#app')
